﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Office.Interop.Excel;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Net.Mail;

namespace WarehouseSpreadsheet
{
    class Program
    {
        public class PLAN
        {
            public int ORDER_FORECAST { get; set; }
            public int CASE_FORECAST { get; set; }
            public int PALLET_FORECAST { get; set; }
            public int ORDERS_PROCESSED { get; set; }
            public int CASES_PROCESSED { get; set; }
            public int PALLETS_PROCESSED { get; set; }
            public DateTime SCHEDULED_SHIP_DATE { get; set; }
            public string COMPANY { get; set; }
            public int STORES_ORDER_FORECAST { get; set; }
            public int STORES_LINE_FORECAST { get; set; }
            public int STORES_UNIT_FORECAST { get; set; }
            public int STORE_ORDERS_PROCESSED { get; set; }
            public int STORE_CASES_PROCESSED { get; set; }
            public int STORE_PALLETS_PROCESSED { get; set; }
            public decimal CASE_FORECAST_DIFFERENCE { get; set; }
            public decimal PALLET_FORECAST_DIFFERENCE { get; set; }
            public decimal CASE_PACK_HOURS { get; set; }
            public decimal CASE_PICK_HOURS { get; set; }
            public decimal PALLET_PICK_HOURS { get; set; }
            public decimal PALLET_PACK_HOURS { get; set; }
            public decimal STORE_CASE_FORECAST_DIFFERENCE { get; set; }
            public decimal STORE_PALLET_FORECAST_DIFFERENCE { get; set; }
        }


        public static decimal GetPicks(DateTime word)
        {
            decimal result;
            if (LLHoursPick.TryGetValue(word, out result))
            {
                return result;
            }
            else
            {
                return 0;
            }
        }

        public static decimal GetPacks(DateTime word)
        {
            decimal result;
            if (LLHoursPack.TryGetValue(word, out result))
            {
                return result;
            }
            else
            {
                return 0;
            }
        }

        public static decimal GetOrders(DateTime word)
        {
            decimal result;
            if (LLOrders.TryGetValue(word, out result))
            {
                return result;
            }
            else
            {
                return 0;
            }
        }

        public static decimal GetCases(DateTime word)
        {
            decimal result;
            if (LLCases.TryGetValue(word, out result))
            {
                return result;
            }
            else
            {
                return 0;
            }
        }

        public static List<PLAN> Plans;

        public static Dictionary<DateTime, decimal> LLOrders;

        public static Dictionary<DateTime, decimal> LLCases;

        public static Dictionary<DateTime, decimal> LLHoursPick;

        public static Dictionary<DateTime, decimal> LLHoursPack;


        static void Main(string[] args)
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory + @"Files\");

            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }

            Plans = BuildPlan();

            int i = 1;

            foreach (var company in Plans.GroupBy(x => x.COMPANY).Select(g => g.First()))
            {
                List<PLAN> splitPlan = Plans.Where(x => x.COMPANY == company.COMPANY).ToList<PLAN>();
                CompanyWorksheet(splitPlan);

                i++;
            }

            LLOrders = new Dictionary<DateTime, decimal>();

            LLCases = new Dictionary<DateTime, decimal>();

            LLHoursPack = new Dictionary<DateTime, decimal>();

            LLHoursPick = new Dictionary<DateTime, decimal>();

            BuildLists();

            SummaryWorksheet(Plans);

            object missing = System.Reflection.Missing.Value;
            object oMissing = System.Reflection.Missing.Value;
            Excel.Application oExcel = new Excel.Application();


            Excel.Application app = new Microsoft.Office.Interop.Excel.Application(AppDomain.CurrentDomain.BaseDirectory + @"Files\KAO B2B.xlsx");

            Excel.Worksheet ws;

            oExcel.Visible = true;

            Excel.Workbooks oBooks = oExcel.Workbooks;
            Excel.Workbook oBook = null;
            Excel.Workbook oBook2 = null;
            Excel.Workbook oBook3 = null;
            Excel.Workbook oBook4 = null;
            Excel.Workbook oBook5 = null;
            Excel.Workbook oBook6 = null;
            Excel.Workbook oBook7 = null;
            Excel.Workbook oBook8 = null;
            Excel.Workbook oBook9 = null;

            oExcel.SheetsInNewWorkbook = 1;
            oExcel.Visible = true;

            oBook = oExcel.Workbooks.Add() as Microsoft.Office.Interop.Excel.Workbook;
            oBook.SaveAs(AppDomain.CurrentDomain.BaseDirectory + @"Files\Pineham WH Planning.xlsx");
            oBook.Close(Type.Missing, Type.Missing, Type.Missing);

            oBook = app.Workbooks.Open(AppDomain.CurrentDomain.BaseDirectory + @"Files\Pineham WH Planning.xlsx", oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing);

            oBook2 = app.Workbooks.Open(AppDomain.CurrentDomain.BaseDirectory + @"Files\KAO B2B.xlsx", oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing);

            oBook3 = app.Workbooks.Open(AppDomain.CurrentDomain.BaseDirectory + @"Files\KPSS B2B.xlsx", oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing);

            oBook4 = app.Workbooks.Open(AppDomain.CurrentDomain.BaseDirectory + @"Files\LighterLife B2B.xlsx", oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing);

            oBook5 = app.Workbooks.Open(AppDomain.CurrentDomain.BaseDirectory + @"Files\Molton Brown B2B.xlsx", oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing);

            oBook6 = app.Workbooks.Open(AppDomain.CurrentDomain.BaseDirectory + @"Files\Perricone B2B.xlsx", oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing);

            oBook7 = app.Workbooks.Open(AppDomain.CurrentDomain.BaseDirectory + @"Files\Production B2B.xlsx", oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing);

            oBook8 = app.Workbooks.Open(AppDomain.CurrentDomain.BaseDirectory + @"Files\Whittard of Chelsea B2B.xlsx", oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing);

            oBook9 = app.Workbooks.Open(AppDomain.CurrentDomain.BaseDirectory + @"Files\Summary.xlsx", oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing, oMissing);

            ws = (Excel.Worksheet)oBook.Worksheets[1];
            oBook2.Worksheets[1].Copy(ws);
            ws = (Excel.Worksheet)oBook.Worksheets[2];
            oBook3.Worksheets[1].Copy(ws);
            ws = (Excel.Worksheet)oBook.Worksheets[3];
            oBook4.Worksheets[1].Copy(ws);
            ws = (Excel.Worksheet)oBook.Worksheets[4];
            oBook5.Worksheets[1].Copy(ws);
            ws = (Excel.Worksheet)oBook.Worksheets[5];
            oBook6.Worksheets[1].Copy(ws);
            ws = (Excel.Worksheet)oBook.Worksheets[6];
            oBook7.Worksheets[1].Copy(ws);
            ws = (Excel.Worksheet)oBook.Worksheets[7];
            oBook8.Worksheets[1].Copy(ws);
            ws = (Excel.Worksheet)oBook.Worksheets[8];
            oBook8.Worksheets[2].Copy(ws);
            ws = (Excel.Worksheet)oBook.Worksheets[9];
            oBook9.Worksheets[1].Copy(ws);

            int theNumber = 0;

            app.DisplayAlerts = false;
            oBook.SaveAs(AppDomain.CurrentDomain.BaseDirectory + @"Files\Pineham WH Planning.xlsx");
            oBook2.Close(Type.Missing, Type.Missing, Type.Missing);
            oBook3.Close(Type.Missing, Type.Missing, Type.Missing);
            oBook4.Close(Type.Missing, Type.Missing, Type.Missing);
            oBook5.Close(Type.Missing, Type.Missing, Type.Missing);
            oBook6.Close(Type.Missing, Type.Missing, Type.Missing);
            oBook7.Close(Type.Missing, Type.Missing, Type.Missing);
            oBook8.Close(Type.Missing, Type.Missing, Type.Missing);
            oBook9.Close(Type.Missing, Type.Missing, Type.Missing);
            oBook.Close(Type.Missing, Type.Missing, Type.Missing);

            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(ws);
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oBook);
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oBook2);
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oBook3);
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oBook4);
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oBook5);
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oBook6);
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oBook7);
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oBook8);
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oBook9);
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oBooks);
            oExcel.Quit();
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oExcel);
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(app);

            oBook = null;
            oBook2 = null;
            oBook3 = null;
            oBook4 = null;
            oBook5 = null;
            oBook6 = null;
            oBook7 = null;
            oBook8 = null;
            oBook9 = null;
            oExcel = null;
            app = null;
            ws = null;

            GC.Collect();
            GC.WaitForPendingFinalizers();

            eMail_ResultB2B();
           eMail_ResultPH(AppDomain.CurrentDomain.BaseDirectory + @"Files\Pineham WH Planning.xlsx", "PINEHAMWHOUSSHEET");
        }

        public static List<PLAN> BuildPlan()
        {
            List<PLAN> thePlan = new List<PLAN>();

            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["ils"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlDataReader dRead = null;
                    cmd.Connection = conn;
                    cmd.CommandText = "WAREHOUSE_PLAN";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    dRead = null;
                    dRead = cmd.ExecuteReader();

                    while (dRead.Read())
                    {
                        PLAN plan = new PLAN();
                        plan.COMPANY = dRead["COMPANY"].ToString();
                        plan.ORDER_FORECAST = Convert.ToInt32(((dRead["ORDER_FORECAST"].ToString() == "") ? "0" : dRead["ORDER_FORECAST"].ToString()));
                        plan.CASE_FORECAST = Convert.ToInt32(((dRead["CASE_FORECAST"].ToString() == "") ? "0" : dRead["CASE_FORECAST"].ToString()));
                        plan.PALLET_FORECAST = Convert.ToInt32(((dRead["PALLET_FORECAST"].ToString() == "") ? "0" : dRead["PALLET_FORECAST"].ToString()));
                        plan.ORDERS_PROCESSED = Convert.ToInt32(((dRead["ORDERS_PROCESSED"].ToString() == "") ? "0" : dRead["ORDERS_PROCESSED"].ToString()));
                        plan.CASES_PROCESSED = Convert.ToInt32(((dRead["CASES_PROCESSED"].ToString() == "") ? "0" : dRead["CASES_PROCESSED"].ToString()));
                        plan.PALLETS_PROCESSED = Convert.ToInt32(((dRead["PALLETS_PROCESSED"].ToString() == "") ? "0" : dRead["PALLETS_PROCESSED"].ToString()));
                        plan.SCHEDULED_SHIP_DATE = Convert.ToDateTime(((dRead["SCHEDULED_SHIP_DATE"].ToString() == "") ? "0" : dRead["SCHEDULED_SHIP_DATE"].ToString()));
                        plan.STORES_ORDER_FORECAST = Convert.ToInt32(((dRead["STORES_ORDER_FORECAST"].ToString() == "") ? "0" : dRead["STORES_ORDER_FORECAST"].ToString()));
                        plan.STORES_LINE_FORECAST = Convert.ToInt32(((dRead["STORES_LINE_FORECAST"].ToString() == "") ? "0" : dRead["STORES_LINE_FORECAST"].ToString()));
                        plan.STORES_UNIT_FORECAST = Convert.ToInt32(((dRead["STORES_UNIT_FORECAST"].ToString() == "") ? "0" : dRead["STORES_UNIT_FORECAST"].ToString()));
                        plan.STORE_ORDERS_PROCESSED = Convert.ToInt32(((dRead["STORE_ORDERS_PROCESSED"].ToString() == "") ? "0" : dRead["STORE_ORDERS_PROCESSED"].ToString()));
                        plan.STORE_CASES_PROCESSED = Convert.ToInt32(((dRead["STORE_CASES_PROCESSED"].ToString() == "") ? "0" : dRead["STORE_CASES_PROCESSED"].ToString()));
                        plan.STORE_PALLETS_PROCESSED = Convert.ToInt32(((dRead["STORE_PALLETS_PROCESSED"].ToString() == "") ? "0" : dRead["STORE_PALLETS_PROCESSED"].ToString()));
                        plan.CASE_FORECAST_DIFFERENCE = Convert.ToDecimal(((dRead["CASE_FORECAST_DIFFERENCE"].ToString() == "") ? "0" : dRead["CASE_FORECAST_DIFFERENCE"].ToString()));
                        plan.PALLET_FORECAST_DIFFERENCE = Convert.ToDecimal(((dRead["PALLET_FORECAST_DIFFERENCE"].ToString() == "") ? "0" : dRead["PALLET_FORECAST_DIFFERENCE"].ToString()));
                        plan.CASE_PACK_HOURS = Convert.ToDecimal(((dRead["CASE_PACK_HOURS"].ToString() == "") ? "0" : dRead["CASE_PACK_HOURS"].ToString()));
                        plan.CASE_PICK_HOURS = Convert.ToDecimal(((dRead["CASE_PICK_HOURS"].ToString() == "") ? "0" : dRead["CASE_PICK_HOURS"].ToString()));
                        plan.PALLET_PICK_HOURS = Convert.ToDecimal(((dRead["PALLET_PICK_HOURS"].ToString() == "") ? "0" : dRead["PALLET_PICK_HOURS"].ToString()));
                        plan.PALLET_PACK_HOURS = Convert.ToDecimal(((dRead["PALLET_PACK_HOURS"].ToString() == "") ? "0" : dRead["PALLET_PACK_HOURS"].ToString()));
                        plan.STORE_CASE_FORECAST_DIFFERENCE = Convert.ToDecimal(((dRead["STORE_CASE_FORECAST_DIFFERENCE"].ToString() == "") ? "0" : dRead["STORE_CASE_FORECAST_DIFFERENCE"].ToString()));
                        plan.STORE_PALLET_FORECAST_DIFFERENCE = Convert.ToDecimal(((dRead["STORE_PALLET_FORECAST_DIFFERENCE"].ToString() == "") ? "0" : dRead["STORE_PALLET_FORECAST_DIFFERENCE"].ToString()));
                        thePlan.Add(plan);
                    }
                    dRead.Dispose();
                }
            }

            return thePlan;
        }

        public static Worksheet CompanyWorksheet(List<PLAN> companyPlan)
        {
            Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            string company = companyPlan.FirstOrDefault().COMPANY;
            xlApp.SheetsInNewWorkbook = ((company == "Whittard of Chelsea") ? 2 : 1);
            xlApp.Visible = true;
            Microsoft.Office.Interop.Excel.Workbook xlWb = xlApp.Workbooks.Add() as Microsoft.Office.Interop.Excel.Workbook;

            Worksheet newCompWS = new Worksheet();
            newCompWS = xlWb.Sheets[1] as Microsoft.Office.Interop.Excel.Worksheet;
            
            newCompWS.Name = company;

            Microsoft.Office.Interop.Excel.Range theRange = newCompWS.get_Range("A1", GetExcelColumnName(companyPlan.Count) + "21");

            theRange[1, 1].Value = company;
            theRange[1, 1].EntireColumn.Font.Bold = true;
            theRange[1, 1].EntireColumn.ColumnWidth = 27;


            theRange[2, 1].Interior.Color = XlRgbColor.rgbSkyBlue;
            theRange[2, 1].EntireRow.NumberFormat = "dd-MMM";
            theRange[3, 1].Interior.Color = XlRgbColor.rgbSkyBlue;

            theRange.Borders.Color = XlRgbColor.rgbBlack;

            newCompWS.get_Range("A4", GetExcelColumnName(companyPlan.Count) + "5").Interior.Color = XlRgbColor.rgbBlack;

            newCompWS.get_Range("A9", GetExcelColumnName(companyPlan.Count) + "11").Interior.Color = XlRgbColor.rgbDarkSeaGreen;

            newCompWS.get_Range("A12", GetExcelColumnName(companyPlan.Count) + "13").Interior.Color = XlRgbColor.rgbWheat;

            newCompWS.get_Range("A17", GetExcelColumnName(companyPlan.Count) + "18").Interior.Color = XlRgbColor.rgbWheat;

            newCompWS.get_Range("A20", GetExcelColumnName(companyPlan.Count) + "21").Interior.Color = XlRgbColor.rgbWheat;
            
            newCompWS.Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

            theRange[4, 1].EntireRow.RowHeight = 5;
            theRange[5, 1].EntireRow.RowHeight = 5;

            theRange[6, 1].Value = "Order Forecast";
            theRange[7, 1].Value = "Case forecast";
            theRange[8, 1].Value = "Pallet Forecast";
            theRange[9, 1].Value = "Actual Orders Processed";
            theRange[10, 1].Value = "Actual Cases Processed";
            theRange[11, 1].Value = "Actual Pallets Processed";
            theRange[12, 1].Value = "Case Forecast Difference";
            theRange[13, 1].Value = "Pallet Forecast Difference";
            
            theRange[14, 1].EntireRow.RowHeight = 5;
            theRange[15, 1].EntireRow.RowHeight = 5;

            newCompWS.get_Range("A14", GetExcelColumnName(companyPlan.Count) + "15").Interior.Color = XlRgbColor.rgbBlack;
            
            theRange[16, 1].Value = "";

            //theRange[17, 1].Value = "Case Picking Hours";
            //theRange[18, 1].Value = "Case Packing Hours";


            //theRange[19, 1].Value = "";
            //theRange[20, 1].Value = "Pallet Picking Hours";
            //theRange[21, 1].Value = "Pallet Packing Hours";
            
            
            newCompWS.Application.ActiveWindow.SplitColumn = 1;
            newCompWS.Application.ActiveWindow.FreezePanes = true;



            int iColumn = 1;
            foreach (PLAN p in companyPlan)
            {
                iColumn++;
                string dayName = p.SCHEDULED_SHIP_DATE.ToString("dddd").Substring(0, 1);
                if (dayName == "S")
                {
                    newCompWS.get_Range(GetExcelColumnName(iColumn) + "2", GetExcelColumnName(iColumn) + "21").Interior.Color = XlRgbColor.rgbGrey;
                    theRange[4, iColumn].Interior.Color = XlRgbColor.rgbBlack;
                    theRange[5, iColumn].Interior.Color = XlRgbColor.rgbBlack;
                    theRange[14, iColumn].Interior.Color = XlRgbColor.rgbBlack;
                    theRange[15, iColumn].Interior.Color = XlRgbColor.rgbBlack;
                }
                theRange[2, iColumn].Value = p.SCHEDULED_SHIP_DATE;
                theRange[3, iColumn].Value = dayName;

                theRange[6, iColumn].Value = p.ORDER_FORECAST;
                theRange[7, iColumn].Value = p.CASE_FORECAST;
                theRange[8, iColumn].Value = p.PALLET_FORECAST;
                theRange[9, iColumn].Value = p.ORDERS_PROCESSED;
                theRange[10, iColumn].Value = p.CASES_PROCESSED;
                theRange[11, iColumn].Value = p.PALLETS_PROCESSED;
                theRange[12, iColumn].Value = p.CASE_FORECAST_DIFFERENCE;
                theRange[13, iColumn].Value = p.PALLET_FORECAST_DIFFERENCE;
                theRange[14, iColumn].Value = "";
                theRange[15, iColumn].Value = "";
                theRange[16, iColumn].Value = "";
                //theRange[17, iColumn].Value = p.CASE_PICK_HOURS;
                //theRange[18, iColumn].Value = p.CASE_PACK_HOURS;
                //theRange[19, iColumn].Value = "";
                //theRange[20, iColumn].Value = p.PALLET_PICK_HOURS;
                //theRange[21, iColumn].Value = p.PALLET_PACK_HOURS;
                

            }
            if (company == "Whittard of Chelsea")
            {
                newCompWS = xlWb.Sheets[2] as Microsoft.Office.Interop.Excel.Worksheet;

                newCompWS.Name = "Whittard Stores";

                theRange = newCompWS.get_Range("A1", GetExcelColumnName(companyPlan.Count) + "21");

                theRange[1, 1].Value = "Whittard Stores";
                theRange[1, 1].EntireColumn.Font.Bold = true;
                theRange[1, 1].EntireColumn.ColumnWidth = 27;


                theRange[2, 1].Interior.Color = XlRgbColor.rgbSkyBlue;
                theRange[2, 1].EntireRow.NumberFormat = "dd-MMM";
                theRange[3, 1].Interior.Color = XlRgbColor.rgbSkyBlue;

                theRange.Borders.Color = XlRgbColor.rgbBlack;

                newCompWS.get_Range("A4", GetExcelColumnName(companyPlan.Count) + "5").Interior.Color = XlRgbColor.rgbBlack;

                newCompWS.get_Range("A9", GetExcelColumnName(companyPlan.Count) + "11").Interior.Color = XlRgbColor.rgbDarkSeaGreen;

                newCompWS.get_Range("A12", GetExcelColumnName(companyPlan.Count) + "13").Interior.Color = XlRgbColor.rgbWheat;

                newCompWS.get_Range("A17", GetExcelColumnName(companyPlan.Count) + "18").Interior.Color = XlRgbColor.rgbWheat;

                newCompWS.get_Range("A20", GetExcelColumnName(companyPlan.Count) + "21").Interior.Color = XlRgbColor.rgbWheat;

                newCompWS.Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                theRange[4, 1].EntireRow.RowHeight = 5;
                theRange[5, 1].EntireRow.RowHeight = 5;

                theRange[6, 1].Value = "Order Forecast";
                theRange[7, 1].Value = "Case forecast";
                theRange[8, 1].Value = "Unit Forecast";
                theRange[9, 1].Value = "Actual Orders Processed";
                theRange[10, 1].Value = "Actual Cases Processed";
                theRange[11, 1].Value = "Actual Units Processed";
                theRange[12, 1].Value = "Case Forecast Difference";
                theRange[13, 1].Value = "Unit Forecast Difference";

                theRange[14, 1].EntireRow.RowHeight = 5;
                theRange[15, 1].EntireRow.RowHeight = 5;

                newCompWS.get_Range("A14", GetExcelColumnName(companyPlan.Count) + "15").Interior.Color = XlRgbColor.rgbBlack;

                theRange[16, 1].Value = "";

                theRange[17, 1].Value = "Case Picking Hours";
                theRange[18, 1].Value = "Case Packing Hours";


                theRange[19, 1].Value = "";
                theRange[20, 1].Value = "Pallet Picking Hours";
                theRange[21, 1].Value = "Pallet Packing Hours";


                newCompWS.Application.ActiveWindow.SplitColumn = 1;
                newCompWS.Application.ActiveWindow.FreezePanes = true;



                iColumn = 1;
                foreach (PLAN p in companyPlan)
                {
                    iColumn++;
                    string dayName = p.SCHEDULED_SHIP_DATE.ToString("dddd").Substring(0, 1);
                    if (dayName == "S")
                    {
                        newCompWS.get_Range(GetExcelColumnName(iColumn) + "2", GetExcelColumnName(iColumn) + "21").Interior.Color = XlRgbColor.rgbGrey;
                        theRange[4, iColumn].Interior.Color = XlRgbColor.rgbBlack;
                        theRange[5, iColumn].Interior.Color = XlRgbColor.rgbBlack;
                        theRange[14, iColumn].Interior.Color = XlRgbColor.rgbBlack;
                        theRange[15, iColumn].Interior.Color = XlRgbColor.rgbBlack;
                    }
                    theRange[2, iColumn].Value = p.SCHEDULED_SHIP_DATE;
                    theRange[3, iColumn].Value = dayName;

                    theRange[6, iColumn].Value = p.STORES_ORDER_FORECAST;
                    theRange[7, iColumn].Value = p.STORES_LINE_FORECAST;
                    theRange[8, iColumn].Value = p.STORES_UNIT_FORECAST;
                    theRange[9, iColumn].Value = p.STORE_ORDERS_PROCESSED;
                    theRange[10, iColumn].Value = p.STORE_CASES_PROCESSED;
                    theRange[11, iColumn].Value = p.STORE_PALLETS_PROCESSED;
                    theRange[12, iColumn].Value = p.CASE_FORECAST_DIFFERENCE;
                    theRange[13, iColumn].Value = p.PALLET_FORECAST_DIFFERENCE;
                    theRange[14, iColumn].Value = "";
                    theRange[15, iColumn].Value = "";
                    theRange[16, iColumn].Value = "";
                    //theRange[17, iColumn].Value = p.CASE_PICK_HOURS;
                    //theRange[18, iColumn].Value = p.CASE_PACK_HOURS;
                    //theRange[19, iColumn].Value = "";
                    //theRange[20, iColumn].Value = "0";
                    //theRange[21, iColumn].Value = "0";


                }
            }
            xlWb.SaveAs(AppDomain.CurrentDomain.BaseDirectory + @"Files\" + company + " B2B.xlsx");
            xlWb.Close(Type.Missing, Type.Missing, Type.Missing);
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(xlWb);
            xlApp.Quit();
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(xlApp);
            xlApp = null;
            
            GC.Collect();
            GC.WaitForPendingFinalizers();
            
            return newCompWS;
        }


        public static Worksheet SummaryWorksheet(List<PLAN> companyPlan)
        {
            Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
            xlApp.SheetsInNewWorkbook = 1;
            xlApp.Visible = true;
            Microsoft.Office.Interop.Excel.Workbook xlWb = xlApp.Workbooks.Add() as Microsoft.Office.Interop.Excel.Workbook;

            Worksheet newCompWS = new Worksheet();
            newCompWS = xlWb.Sheets[1] as Microsoft.Office.Interop.Excel.Worksheet;
            string company = "Summary";
            newCompWS.Name = company;

            Microsoft.Office.Interop.Excel.Range theRange = newCompWS.get_Range("A1", GetExcelColumnName(companyPlan.Count) + "90");

            theRange[1, 1].Value = "Orders";
            theRange[1, 1].EntireColumn.Font.Bold = true;
            theRange[1, 1].EntireColumn.ColumnWidth = 27;


            theRange[2, 1].Interior.Color = XlRgbColor.rgbSkyBlue;
            theRange[2, 1].EntireRow.NumberFormat = "dd-MMM";
            theRange[3, 1].Interior.Color = XlRgbColor.rgbSkyBlue;

            theRange.Borders.Color = XlRgbColor.rgbBlack;

            theRange[24, 1].EntireColumn.Font.Bold = true;

            newCompWS.Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

            theRange[4, 1].Value = "KAO";
            theRange[5, 1].Value = "KPSS";
            theRange[6, 1].Value = "LighterLife";
            theRange[7, 1].Value = "Molton Brown";
            theRange[8, 1].Value = "Perricone";
            theRange[9, 1].Value = "Production";
            theRange[10, 1].Value = "Whittard of Chelsea";

            theRange[13, 1].Value = "Cases";
            theRange[13, 1].EntireColumn.Font.Bold = true;
            theRange[13, 1].EntireColumn.ColumnWidth = 27;

            theRange[14, 1].Interior.Color = XlRgbColor.rgbSkyBlue;
            theRange[14, 1].EntireRow.NumberFormat = "dd-MMM";
            theRange[15, 1].Interior.Color = XlRgbColor.rgbSkyBlue;
            theRange[14, 1].EntireRow.RowHeight = 5;
            theRange[15, 1].EntireRow.RowHeight = 5;

            theRange[16, 1].Value = "KAO";
            theRange[17, 1].Value = "KPSS";
            theRange[18, 1].Value = "LighterLife";
            theRange[19, 1].Value = "Molton Brown";
            theRange[20, 1].Value = "Perricone";
            theRange[21, 1].Value = "Production";
            theRange[22, 1].Value = "Whittard of Chelsea";

            theRange[24, 1].Value = "Case Picking";
            theRange[24, 1].EntireColumn.Font.Bold = true;
            theRange[24, 1].EntireColumn.ColumnWidth = 27;

            theRange[25, 1].Interior.Color = XlRgbColor.rgbSkyBlue;
            theRange[25, 1].EntireRow.NumberFormat = "dd-MMM";
            theRange[26, 1].Interior.Color = XlRgbColor.rgbSkyBlue;
            theRange[25, 1].EntireRow.RowHeight = 5;
            theRange[26, 1].EntireRow.RowHeight = 5;

            theRange[27, 1].Value = "KAO";
            theRange[28, 1].Value = "KPSS";
            theRange[29, 1].Value = "LighterLife";
            theRange[30, 1].Value = "Molton Brown";
            theRange[31, 1].Value = "Perricone";
            theRange[32, 1].Value = "Production";
            theRange[33, 1].Value = "Whittard of Chelsea";

            theRange[35, 1].Value = "Pallet Picking";
            theRange[35, 1].EntireColumn.Font.Bold = true;
            theRange[35, 1].EntireColumn.ColumnWidth = 27;


            theRange[36, 1].Interior.Color = XlRgbColor.rgbSkyBlue;
            theRange[36, 1].EntireRow.NumberFormat = "dd-MMM";
            theRange[37, 1].Interior.Color = XlRgbColor.rgbSkyBlue;
            theRange[36, 1].EntireRow.RowHeight = 5;
            theRange[37, 1].EntireRow.RowHeight = 5;

            theRange[38, 1].Value = "KAO";
            theRange[39, 1].Value = "KPSS";
            theRange[40, 1].Value = "LighterLife";
            theRange[41, 1].Value = "Molton Brown";
            theRange[42, 1].Value = "Perricone";
            theRange[43, 1].Value = "Production";
            theRange[44, 1].Value = "Whittard of Chelsea";

            theRange[46, 1].Value = "Packing";
            theRange[46, 1].EntireColumn.Font.Bold = true;
            theRange[46, 1].EntireColumn.ColumnWidth = 27;


            theRange[47, 1].Interior.Color = XlRgbColor.rgbSkyBlue;
            theRange[47, 1].EntireRow.NumberFormat = "dd-MMM";
            theRange[48, 1].Interior.Color = XlRgbColor.rgbSkyBlue;
            theRange[47, 1].EntireRow.RowHeight = 5;
            theRange[48, 1].EntireRow.RowHeight = 5;

            theRange[49, 1].Value = "KAO";
            theRange[50, 1].Value = "KPSS";
            theRange[51, 1].Value = "LighterLife";
            theRange[52, 1].Value = "Molton Brown";
            theRange[53, 1].Value = "Perricone";
            theRange[54, 1].Value = "Production";
            theRange[55, 1].Value = "Whittard of Chelsea";

            theRange[57, 1].Value = "Total Hours";
            theRange[57, 1].EntireColumn.Font.Bold = true;
            theRange[57, 1].EntireColumn.ColumnWidth = 27;


            theRange[58, 1].Interior.Color = XlRgbColor.rgbSkyBlue;
            theRange[58, 1].EntireRow.NumberFormat = "dd-MMM";
            theRange[59, 1].Interior.Color = XlRgbColor.rgbSkyBlue;
            theRange[58, 1].EntireRow.RowHeight = 5;
            theRange[59, 1].EntireRow.RowHeight = 5;

            theRange[60, 1].Value = "KAO";
            theRange[61, 1].Value = "KPSS";
            theRange[62, 1].Value = "LighterLife";
            theRange[63, 1].Value = "Molton Brown";
            theRange[64, 1].Value = "Perricone";
            theRange[65, 1].Value = "Production";
            theRange[66, 1].Value = "Whittard of Chelsea";

            theRange[68, 1].Value = "Total Direct";

            theRange[70, 1].Value = "Indirect Labour";
            theRange[70, 1].EntireColumn.Font.Bold = true;
            theRange[70, 1].EntireColumn.ColumnWidth = 27;


            theRange[71, 1].Interior.Color = XlRgbColor.rgbSkyBlue;
            theRange[71, 1].EntireRow.NumberFormat = "dd-MMM";
            theRange[72, 1].Interior.Color = XlRgbColor.rgbSkyBlue;
            theRange[71, 1].EntireRow.RowHeight = 5;
            theRange[72, 1].EntireRow.RowHeight = 5;

            theRange[73, 1].Value = "Drivers bendi replens";
            theRange[74, 1].Value = "Hilop mezz whs";
            theRange[75, 1].Value = "Goods Out";
            theRange[76, 1].Value = "Goods In";
            theRange[77, 1].Value = "Inventory";
            theRange[78, 1].Value = "Yard- whs cleaners ";
            theRange[79, 1].Value = "CB driver mezz/ yard";
            theRange[80, 1].Value = "Returns";
            theRange[81, 1].Value = "Transport Admin";
            theRange[82, 1].Value = "Bookings ";
            theRange[83, 1].Value = "DGN";
            theRange[84, 1].Value = "QC";


            theRange[86, 1].Value = "Total Indirect";

            theRange[90, 1].Value = "Grand Total";

            newCompWS.Application.ActiveWindow.SplitColumn = 1;
            newCompWS.Application.ActiveWindow.FreezePanes = true;



            int iColumn = 1;
            string compo = "";
            var scheds = companyPlan.GroupBy(x => x.SCHEDULED_SHIP_DATE)
                                    .Select(grp => grp.First())
                                    .ToList();
            foreach (var sched in scheds)
            {
                iColumn++;
                foreach (PLAN p in companyPlan.Where(x => x.SCHEDULED_SHIP_DATE == sched.SCHEDULED_SHIP_DATE).OrderBy(x => x.COMPANY))
                {

                    if (p.COMPANY != "Revolution")
                    {

                        int range1 = 0;
                        int range2 = 0;
                        int range3 = 0;
                        int range4 = 0;
                        int range5 = 0;
                        int range6 = 0;

                        string dayName = p.SCHEDULED_SHIP_DATE.ToString("dddd").Substring(0, 1);

                        theRange[2, iColumn].Value = p.SCHEDULED_SHIP_DATE;
                        theRange[3, iColumn].Value = dayName;

                        if (dayName == "S")
                        {
                            newCompWS.get_Range(GetExcelColumnName(iColumn) + "2", GetExcelColumnName(iColumn) + "90").Interior.Color = XlRgbColor.rgbGrey;
                        }

                        dayName = "";

                        switch (p.COMPANY)
                        {
                            case "KAO":
                                range1 = 4;
                                range2 = 16;
                                range3 = 27;
                                range4 = 38;
                                range5 = 49;
                                range6 = 60;
                                break;
                            case "KPSS":
                                range1 = 5;
                                range2 = 17;
                                range3 = 28;
                                range4 = 39;
                                range5 = 50;
                                range6 = 61;
                                break;
                            case "LighterLife":
                                range1 = 6;
                                range2 = 18;
                                range3 = 29;
                                range4 = 40;
                                range5 = 51;
                                range6 = 62;
                                break;
                            case "Molton Brown":
                                range1 = 7;
                                range2 = 19;
                                range3 = 30;
                                range4 = 41;
                                range5 = 52;
                                range6 = 63;
                                break;
                            case "Perricone":
                                range1 = 8;
                                range2 = 20;
                                range3 = 31;
                                range4 = 42;
                                range5 = 53;
                                range6 = 64;
                                break;
                            case "Production":
                                range1 = 9;
                                range2 = 21;
                                range3 = 32;
                                range4 = 43;
                                range5 = 54;
                                range6 = 65;
                                break;
                            case "Whittard of Chelsea":
                                range1 = 10;
                                range2 = 22;
                                range3 = 33;
                                range4 = 44;
                                range5 = 55;
                                range6 = 66;
                                break;
                        }

                        theRange[range1, iColumn].Value = ((p.ORDERS_PROCESSED == 0) ? p.ORDER_FORECAST : p.ORDERS_PROCESSED) + (p.COMPANY != "LighterLife" ? 0 : GetOrders(p.SCHEDULED_SHIP_DATE));
                        theRange[range2, iColumn].Value = ((p.CASES_PROCESSED == 0) ? p.CASE_FORECAST : p.CASES_PROCESSED) + (p.COMPANY != "LighterLife" ? 0 : GetCases(p.SCHEDULED_SHIP_DATE));
                        theRange[range3, iColumn].Value = p.CASE_PICK_HOURS + (p.COMPANY != "LighterLife" ? 0 : GetPicks(p.SCHEDULED_SHIP_DATE));
                        theRange[range4, iColumn].Value = p.PALLET_PICK_HOURS;
                        theRange[range5, iColumn].Value = p.CASE_PACK_HOURS + p.PALLET_PACK_HOURS + (p.COMPANY != "LighterLife" ? 0 : GetPacks(p.SCHEDULED_SHIP_DATE));
                        theRange[range6, iColumn].Value = p.CASE_PACK_HOURS + p.PALLET_PACK_HOURS + p.CASE_PICK_HOURS + p.PALLET_PICK_HOURS + (p.COMPANY != "LighterLife" ? 0 : GetPacks(p.SCHEDULED_SHIP_DATE)) + (p.COMPANY != "LighterLife" ? 0 : GetPicks(p.SCHEDULED_SHIP_DATE));
                        theRange[68, iColumn].Value = "=SUM(" + GetExcelColumnName(iColumn) + "27:" + GetExcelColumnName(iColumn) + "33)+SUM(" + GetExcelColumnName(iColumn) + "38:" + GetExcelColumnName(iColumn) + "44)+SUM(" + GetExcelColumnName(iColumn) + "49:" + GetExcelColumnName(iColumn) + "55)";

                        theRange[73, iColumn].Value = 30;
                        theRange[74, iColumn].Value = 15;
                        theRange[75, iColumn].Value = 22.5;
                        theRange[76, iColumn].Value = 22.5;
                        theRange[77, iColumn].Value = 75;
                        theRange[78, iColumn].Value = 22.5;
                        theRange[79, iColumn].Value = 15;
                        theRange[80, iColumn].Value = 15;
                        theRange[81, iColumn].Value = 22.5;
                        theRange[82, iColumn].Value = 7.5;
                        theRange[83, iColumn].Value = 7.5;
                        theRange[84, iColumn].Value = 15;


                        theRange[86, iColumn].Value = "=SUM(" + GetExcelColumnName(iColumn) + "73:" + GetExcelColumnName(iColumn) + "84)";
                        theRange[90, iColumn].Value = "=" + GetExcelColumnName(iColumn) + "68+" + GetExcelColumnName(iColumn) + "86";
                    }
                }

            }
            xlWb.SaveAs(AppDomain.CurrentDomain.BaseDirectory + @"Files\Summary.xlsx");
            xlWb.Close(Type.Missing, Type.Missing, Type.Missing);
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(xlWb);
            xlApp.Quit();
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(xlApp);

            return newCompWS;
        }

        private static string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }

        public static void BuildLists()
        {
            List<PLAN> thePlan = new List<PLAN>();
            LLHoursPack.Clear();
            LLHoursPick.Clear();
            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["ils"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlDataReader dRead = null;
                    cmd.Connection = conn;
                    cmd.CommandText = "ECOM_LL_HOURS";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    conn.Open();
                    dRead = null;
                    dRead = cmd.ExecuteReader();

                    while (dRead.Read())
                    {
                        LLOrders.Add(Convert.ToDateTime(dRead["SCHEDULED_SHIP_DATE"].ToString()), Convert.ToDecimal(((dRead["ORDERS_PREDICT"].ToString() == "") ? "0" : dRead["ORDERS_PREDICT"].ToString())));
                        LLCases.Add(Convert.ToDateTime(dRead["SCHEDULED_SHIP_DATE"].ToString()), Convert.ToDecimal(((dRead["CASES_PREDICT"].ToString() == "") ? "0" : dRead["ORDERS_PREDICT"].ToString())));
                        LLHoursPick.Add(Convert.ToDateTime(dRead["SCHEDULED_SHIP_DATE"].ToString()), Convert.ToDecimal(((dRead["PICK_HOURS"].ToString() == "") ? "0" : dRead["PICK_HOURS"].ToString())));
                        LLHoursPack.Add(Convert.ToDateTime(dRead["SCHEDULED_SHIP_DATE"].ToString()), Convert.ToDecimal(((dRead["PACK_HOURS"].ToString() == "") ? "0" : dRead["PACK_HOURS"].ToString())));
                    }
                }
            }
        }

        public static void eMail_ResultB2B()
        {

            Dictionary<string, string> companies = new Dictionary<string, string>();

            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["globe"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlDataReader dRead = null;
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT DISTINCT REPLACE(VALUE,'B2B','') IDENTIFIER1 FROM GENERIC_CONFIG_DETAIL WHERE RECORD_TYPE = 'EMAIL' AND VALUE LIKE 'B2B%' AND VALUE NOT LIKE '%-FILE'";

                    conn.Open();
                    dRead = null;
                    dRead = cmd.ExecuteReader();

                    while (dRead.Read())
                    {
                        companies.Add(dRead["IDENTIFIER1"].ToString(), dRead["IDENTIFIER1"].ToString());
                    }
                    dRead.Dispose();
                }
            }

            foreach (KeyValuePair<string, string> entry in companies)
            {
                if (entry.Value != "Arbonne" || entry.Value == "Arbonne" && DateTime.Now.Day == 15)
                {
                    MailMessage eMail = new MailMessage();

                    using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["globe"].ConnectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            SqlDataReader dRead = null;
                            cmd.Connection = conn;
                            cmd.CommandText = "SELECT IDENTIFIER1 FROM GENERIC_CONFIG_DETAIL WHERE RECORD_TYPE = 'EMAIL' AND VALUE = 'B2B' + @Company";
                            cmd.Parameters.AddWithValue("@Company", entry.Value);
                            conn.Open();
                            dRead = null;
                            dRead = cmd.ExecuteReader();

                            while (dRead.Read())
                            {
                                eMail.To.Add(new MailAddress(dRead["IDENTIFIER1"].ToString()));
                            }
                            dRead.Dispose();
                        }
                    }

                    eMail.Subject = entry.Value + " B2B Control Sheet";
                    eMail.IsBodyHtml = true;
                    Attachment attach = new Attachment(AppDomain.CurrentDomain.BaseDirectory + @"Files\" + entry.Value + " B2B.xlsx");
                    eMail.Attachments.Add(attach);
                    SmtpClient eMailSend = new SmtpClient();

                    eMailSend.Send(eMail);

                    eMailSend.Dispose();
                    eMail.Dispose();

                    eMailSend = null;
                    eMail = null;
                }
            }

        }

        public static void eMail_ResultPH(string path, string type)
        {

            MailMessage eMail = new MailMessage();

            using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["globe"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlDataReader dRead = null;
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT IDENTIFIER1 FROM GENERIC_CONFIG_DETAIL WHERE RECORD_TYPE = 'EMAIL' AND VALUE = '" + type + "'";

                    conn.Open();
                    dRead = null;
                    dRead = cmd.ExecuteReader();

                    while (dRead.Read())
                    {
                        eMail.To.Add(new MailAddress(dRead["IDENTIFIER1"].ToString()));
                    }
                    dRead.Dispose();
                }
            }


            eMail.Subject = "Pineham " + ((type == "PINEHAMWHOUSSHEET") ? "Warehousing" : "Ecomm") + " Sheet";
            eMail.IsBodyHtml = true;
            Attachment attach = new Attachment(path);
            eMail.Attachments.Add(attach);
            SmtpClient eMailSend = new SmtpClient();

            eMailSend.Send(eMail);

            eMailSend.Dispose();
            eMail.Dispose();

            eMailSend = null;
            eMail = null;

            FileInfo fi = new FileInfo(path);
            string newFile = @"\\uknndc03\dalepak\dalepak\Pineham\Warehousing\Planning\" + Path.GetFileNameWithoutExtension(path) + " " + DateTime.Now.ToString("yyyy-MM-dd") + fi.Extension;
            File.Copy(path, newFile);
        }
    }
}
